package cl.entel.tde.integration.soa.model;

import java.util.ArrayList;
import java.util.List;

public class SystemModel {
    private Long id;

    private String code;

    private String name;

    private String description;

    private List<SystemApiModel> apis;

    public SystemModel() {
        this.apis = new ArrayList<SystemApiModel>();
    }

    public SystemModel(Long id, String code, String name, String description) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.description = description;
        this.apis = new ArrayList<SystemApiModel>();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<SystemApiModel> getApis() {
        return apis;
    }

    public void setApis(List<SystemApiModel> apis) {
        this.apis = apis;
    }
}
