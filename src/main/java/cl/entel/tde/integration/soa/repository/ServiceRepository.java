package cl.entel.tde.integration.soa.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import cl.entel.tde.integration.soa.domain.*;

@Repository
public interface ServiceRepository extends CrudRepository<Service, Long> {

    public List<Service> findByNameLikeAndCodeLike(String name, String code);

    @Query("SELECT DISTINCT(s) FROM Service s JOIN s.capabilities c WHERE c.name like ?3 and s.name like ?1 and s.code like ?2")
    public List<Service> find(String servicio, String code, String capability);

}
