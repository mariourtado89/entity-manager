package cl.entel.tde.integration.soa.controller;

import cl.entel.tde.integration.soa.model.EnterpriseModel;
import cl.entel.tde.integration.soa.service.EnterpriseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/entity/enterprise")
public class EnterpriseController {

    @Autowired
    private EnterpriseService enterpriseService;

    public EnterpriseController() {
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public List<EnterpriseModel> find(@RequestParam(value = "code", defaultValue = "*", required = false) String code){
        return enterpriseService.find(code);
    }

}
