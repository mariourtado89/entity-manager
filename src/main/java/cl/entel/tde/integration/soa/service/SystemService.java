package cl.entel.tde.integration.soa.service;

import cl.entel.tde.integration.soa.domain.System;
import cl.entel.tde.integration.soa.model.SystemModel;
import cl.entel.tde.integration.soa.model.factory.SystemFactory;
import cl.entel.tde.integration.soa.repository.SystemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import cl.entel.tde.integration.soa.domain.*;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class SystemService {

    @Autowired
    private SystemRepository systemRepository;

    @Autowired
    private SystemFactory systemFactory;

    public List<SystemModel> find(String code, boolean expand){
        code = code.replace("*", "%");
        List<System> systems = this.systemRepository.findByCodeLike(code);
        List<SystemModel> providers = systems.stream().map(x-> systemFactory.map(x, expand)).collect(Collectors.toList());
        return providers;
    }

    public SystemService() {
    }

    @Cacheable("service-system-getAll")
    public List<SystemModel> getAll(){
        return StreamSupport.stream(systemRepository.findAll().spliterator(), false).map(x-> systemFactory.map(x, false)).sorted(Comparator.comparing(SystemModel::getCode)).collect(Collectors.toList());
    }

    @Cacheable("service-system-get-id")
    public SystemModel get(Long id){
        System system = systemRepository.findById(id).get();
        return systemFactory.map(system, true);
    }
}
