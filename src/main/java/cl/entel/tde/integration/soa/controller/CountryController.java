package cl.entel.tde.integration.soa.controller;

import cl.entel.tde.integration.soa.model.CountryModel;
import cl.entel.tde.integration.soa.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/entity/country")
public class CountryController {

    @Autowired
    private CountryService enterpriseService;

    public CountryController() {
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public List<CountryModel> find(@RequestParam(value = "code", defaultValue = "*", required = false) String code){
        return enterpriseService.find(code);
    }

}
