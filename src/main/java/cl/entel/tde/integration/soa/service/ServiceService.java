package cl.entel.tde.integration.soa.service;

import cl.entel.tde.integration.soa.model.ServiceModel;
import cl.entel.tde.integration.soa.model.factory.ServiceFactory;
import cl.entel.tde.integration.soa.repository.CapabilityRepository;
import cl.entel.tde.integration.soa.repository.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;
import java.util.stream.Collectors;
import cl.entel.tde.integration.soa.domain.*;

@org.springframework.stereotype.Service
public class ServiceService {

    @Autowired
    private ServiceRepository serviceRepository;

    @Autowired
    private ServiceFactory serviceFactory;

    @Autowired
    private CapabilityRepository capabilityRepository;

    @Cacheable("service-service-get-id")
    public ServiceModel get (Long id){
        Service service = serviceRepository.findById(id).get();
        return serviceFactory.map(service);
    }

    public List<ServiceModel> find (String name, String code){
        List<Service> services = serviceRepository.findByNameLikeAndCodeLike(name, code);
        return services.stream().map( x -> serviceFactory.map(x)).collect(Collectors.toList());
    }

    public List<ServiceModel> find (String service, String code, String capability){
        service = service.replace("*", "%");
        code = code.replace("*", "%");
        capability = capability.replace("*", "%");
        List<Service> services = serviceRepository.find(service, code, capability);
        return services.stream().map( x -> serviceFactory.map(x)).collect(Collectors.toList());
    }
}
