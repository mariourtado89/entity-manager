package cl.entel.tde.integration.soa.model;

public class SystemApiProvider {

    private String code;

    private String name;

    private String description;

    public SystemApiProvider() {
    }


    public SystemApiProvider(String code, String name, String description) {
        this.code = code;
        this.name = name;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
