package cl.entel.tde.integration.soa.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import cl.entel.tde.integration.soa.domain.*;

@Repository
public interface SystemApiOperationRepository extends CrudRepository<SystemApiOperation, Long> {

    //@Query("select sao from from System s, SystemApi sa, SystemApiOperation sao where  s.code = ?1 and sa.code = ?2 and sao.name = ?3 and sao.version = ?4")
    @Query("select sao from System s join s.apis  sa join sa.operations sao where  s.code = ?1 and sa.code = ?2 and sao.name = ?3 and sao.version = ?4")
    public SystemApiOperation findByTarget(String system, String api, String operation, String version);
}
