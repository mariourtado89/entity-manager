package cl.entel.tde.integration.soa.model.Configuration;

import java.util.Map;

public class Configuration {

    private String key;

    private boolean hasValues;

    private Map<String, String> values;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isHasValues() {
        return hasValues;
    }

    public void setHasValues(boolean hasValues) {
        this.hasValues = hasValues;
    }

    public Map<String, String> getValues() {
        return values;
    }

    public void setValues(Map<String, String> values) {
        this.values = values;
    }
}
