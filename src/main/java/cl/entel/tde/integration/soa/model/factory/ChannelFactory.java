package cl.entel.tde.integration.soa.model.factory;

import cl.entel.tde.integration.soa.model.ChannelModel;
import org.springframework.stereotype.Component;
import cl.entel.tde.integration.soa.domain.*;

@Component
public class ChannelFactory {

    public ChannelFactory() {
    }

    public ChannelModel marshall(Channel channel){
        return new ChannelModel(channel.getId(), channel.getMode(), channel.getName());
    }

}
