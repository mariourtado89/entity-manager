package cl.entel.tde.integration.soa.model.factory;

import cl.entel.tde.integration.soa.model.CountryModel;
import org.springframework.stereotype.Component;
import cl.entel.tde.integration.soa.domain.*;

@Component
public class CountryFactory {


    public CountryFactory() {
    }

    public CountryModel marshall(Country country){
        return new CountryModel(country.getId(), country.getCode(), country.getName());
    }
}
