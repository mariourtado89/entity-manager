package cl.entel.tde.integration.soa.model;

public class ChannelModel {

    private Long id;

    private String mode;

    private String name;

    public ChannelModel(Long id, String mode, String name) {
        this.id = id;
        this.mode = mode;
        this.name = name;
    }

    public ChannelModel() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
