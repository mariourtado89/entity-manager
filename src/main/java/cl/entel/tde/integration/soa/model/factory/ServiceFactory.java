package cl.entel.tde.integration.soa.model.factory;

import cl.entel.tde.integration.soa.model.ServiceModel;
import cl.entel.tde.integration.soa.model.ServiceOperation;
import org.springframework.stereotype.Component;
import cl.entel.tde.integration.soa.domain.*;

@Component
public class ServiceFactory {

    public ServiceFactory() {
    }

    public ServiceModel map(Service service){
        ServiceModel model = new ServiceModel(service.getId(),service.getName(), service.getCode());
        service.getCapabilities().forEach( x-> model.addCapabilities(this.map(x)) );
        return model;
    }

    public ServiceOperation map(Capability capability){
        ServiceOperation operation = new ServiceOperation(capability.getId(), capability.getName(), capability.getCode());
        return operation;

    }
}
