package cl.entel.tde.integration.soa.model.factory;

import cl.entel.tde.integration.soa.model.EnterpriseModel;
import org.springframework.stereotype.Component;
import cl.entel.tde.integration.soa.domain.*;

@Component
public class EnterpriseFactory {

    public EnterpriseFactory() {
    }

    public EnterpriseModel marshal(Enterprise enterprise){
        return new EnterpriseModel(enterprise.getId(), enterprise.getCode(), enterprise.getName());
    }
}
