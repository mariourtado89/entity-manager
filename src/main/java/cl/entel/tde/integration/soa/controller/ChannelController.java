package cl.entel.tde.integration.soa.controller;

import cl.entel.tde.integration.soa.model.ChannelModel;
import cl.entel.tde.integration.soa.service.ChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/entity/channel")
public class ChannelController {

    @Autowired
    private ChannelService channelService;

    public ChannelController() {
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public List<ChannelModel> find(@RequestParam(value = "mode", required = false, defaultValue = "*") String mode, @RequestParam(value = "name", required = false, defaultValue = "*") String name){
        return channelService.find();
    }

}
