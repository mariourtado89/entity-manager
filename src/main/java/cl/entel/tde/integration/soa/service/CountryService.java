package cl.entel.tde.integration.soa.service;


import cl.entel.tde.integration.soa.model.CountryModel;
import cl.entel.tde.integration.soa.model.factory.CountryFactory;
import cl.entel.tde.integration.soa.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CountryService {

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private CountryFactory countryFactory;

    public CountryService() {
    }

    public List<CountryModel> find(String code){
        code = code.replace("*", "%");
        return countryRepository.findByCodeLike(code).stream().map(x-> countryFactory.marshall(x)).collect(Collectors.toList());
    }
}
