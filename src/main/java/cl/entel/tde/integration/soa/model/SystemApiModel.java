package cl.entel.tde.integration.soa.model;

import java.util.ArrayList;
import java.util.List;

public class SystemApiModel {

    private Long id;

    private String code;

    private String name;

    private String description;

    private List<SystemApiOperationModel> operations;

    public SystemApiModel() {
        this.operations = new ArrayList<SystemApiOperationModel>();
    }

    public SystemApiModel(Long id, String code, String name, String description) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.description = description;
        this.operations = new ArrayList<SystemApiOperationModel>();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<SystemApiOperationModel> getOperations() {
        return operations;
    }

    public void setOperations(List<SystemApiOperationModel> operations) {
        this.operations = operations;
    }
}
