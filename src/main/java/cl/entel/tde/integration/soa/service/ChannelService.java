package cl.entel.tde.integration.soa.service;

import cl.entel.tde.integration.soa.model.ChannelModel;
import cl.entel.tde.integration.soa.model.factory.ChannelFactory;
import cl.entel.tde.integration.soa.repository.ChannelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import cl.entel.tde.integration.soa.domain.*;

@Service
public class ChannelService {

    @Autowired
    private ChannelRepository channelRepository;

    @Autowired
    private ChannelFactory channelFactory;

    public ChannelService() {
    }

    public List<ChannelModel> find(){
        return StreamSupport.stream(channelRepository.findAll().spliterator(), false).map(x-> channelFactory.marshall(x)).collect(Collectors.toList());
    }
}
