package cl.entel.tde.integration.soa.service;

import cl.entel.tde.integration.soa.model.EnterpriseModel;
import cl.entel.tde.integration.soa.model.factory.EnterpriseFactory;
import cl.entel.tde.integration.soa.repository.EnterpriseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EnterpriseService {


    @Autowired
    private EnterpriseRepository enterpriseRepository;

    @Autowired
    private EnterpriseFactory enterpriseFactory;

    public EnterpriseService() {
    }

    public List<EnterpriseModel> find(String code){
        code = code.replace("*", "%");
        return enterpriseRepository.findByCodeLike(code).stream().map(x -> enterpriseFactory.marshal(x)).collect(Collectors.toList());
    }
}
