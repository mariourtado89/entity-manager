package cl.entel.tde.integration.soa.repository;

import cl.entel.tde.integration.soa.domain.Capability;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CapabilityRepository extends CrudRepository<Capability, Long> {

    public List<Capability> findByNameLikeAndCodeLike(String name, String code);

    @Query("select c from Capability c where c.name like ?3 and c.service.name like ?1 and c.service.code like ?2")
    public List<Capability> find(String servicio, String code, String capability);

}
