package cl.entel.tde.integration.soa.controller;

import cl.entel.tde.integration.soa.model.SystemModel;
import cl.entel.tde.integration.soa.service.SystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/systems")
@CrossOrigin(origins = "http://localhost:3000")
public class SystemController {

    @Autowired
    private SystemService systemService;

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<SystemModel>> list(@RequestParam(value = "code", defaultValue = "*", required = false) String code, @RequestParam(value = "expand", required = false, defaultValue = "false") boolean expand) {
        List<SystemModel> providers = systemService.find(code, expand);
        return new ResponseEntity<>(providers, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<SystemModel> get(@PathVariable(name = "id") Long id){
        SystemModel system = systemService.get(id);
        return new ResponseEntity<>(system, HttpStatus.OK);
    }
}
