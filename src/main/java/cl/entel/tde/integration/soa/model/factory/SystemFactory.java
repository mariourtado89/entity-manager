package cl.entel.tde.integration.soa.model.factory;

import cl.entel.tde.integration.soa.domain.*;
import cl.entel.tde.integration.soa.domain.System;
import cl.entel.tde.integration.soa.model.SystemApiModel;
import cl.entel.tde.integration.soa.model.SystemApiOperationModel;
import cl.entel.tde.integration.soa.model.SystemModel;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class SystemFactory {

     public SystemFactory() {
    }

    public SystemModel map(System system, boolean expand){
        SystemModel provider = new SystemModel(system.getId(),system.getCode(), system.getName(), system.getDescription());
        if (expand){
            List<SystemApiModel> apis = system.getApis().stream().map(x-> this.map(x)).collect(Collectors.toList());
            provider.setApis(apis);
        }
        return provider;
    }

    public SystemApiModel map(SystemApi api){
        SystemApiModel model = new SystemApiModel(api.getId(), api.getName(), api.getCode(), api.getDescription());
        List<SystemApiOperationModel> operations = api.getOperations().stream().map(x-> this.map(x)).collect(Collectors.toList());
        model.setOperations(operations);
        return model;
    }

    public SystemApiOperationModel map (SystemApiOperation operation){
        SystemApiOperationModel model = new SystemApiOperationModel(operation.getId(), operation.getName(), operation.getVersion(), operation.getDescription());
        return model;
    }


}
