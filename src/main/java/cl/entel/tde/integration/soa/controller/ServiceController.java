package cl.entel.tde.integration.soa.controller;

import cl.entel.tde.integration.soa.model.ServiceModel;
import cl.entel.tde.integration.soa.service.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping(value = "/services")
@CrossOrigin(origins = "http://localhost:3000")
public class ServiceController {

    @Autowired
    private ServiceService serviceService;

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<ServiceModel>> find(@RequestParam(required = false, name = "service", defaultValue = "*") String service, @RequestParam(required = false, name = "code", defaultValue = "*") String code, @RequestParam(name = "capability", required = false, defaultValue = "*") String capability){
        List<ServiceModel> services = serviceService.find(service, code, capability);
        return new ResponseEntity<>(services, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<ServiceModel> find(@PathVariable("id") Long id){
        ServiceModel service = serviceService.get(id);
        return new ResponseEntity<>(service, HttpStatus.OK);
    }

}
