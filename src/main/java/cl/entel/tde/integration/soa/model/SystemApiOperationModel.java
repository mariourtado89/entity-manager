package cl.entel.tde.integration.soa.model;

public class SystemApiOperationModel {

    private Long id;

    private String name;

    private String version;

    private String description;

    public SystemApiOperationModel() {
    }

    public SystemApiOperationModel(Long id, String name, String version, String description) {
        this.id = id;
        this.name = name;
        this.version = version;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
