package cl.entel.tde.integration.soa.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import cl.entel.tde.integration.soa.domain.*;

@Repository
public interface EnterpriseRepository extends CrudRepository<Enterprise, Long> {

    public List<Enterprise> findByCodeLike(String code);
}
