package cl.entel.tde.integration.soa.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import cl.entel.tde.integration.soa.domain.*;

import java.util.List;

@Repository
public interface CountryRepository extends CrudRepository<Country, Long> {


    public List<Country> findByCodeLike(String code);
}
