package cl.entel.tde.integration.soa.model;

import java.util.ArrayList;
import java.util.List;

public class ServiceModel {

    private Long id;

    private String name;

    private String code;

    private List<ServiceOperation> capabilities;

    public ServiceModel(Long id, String name, String code) {
        this.name = name;
        this.code = code;
        this.capabilities = new ArrayList<ServiceOperation>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<ServiceOperation> getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(List<ServiceOperation> capabilities) {
        this.capabilities = capabilities;
    }

    public void addCapabilities(ServiceOperation capability) {
        this.capabilities.add(capability);
    }
}
