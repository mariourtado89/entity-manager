package cl.entel.tde.integration.soa.repository;

import cl.entel.tde.integration.soa.domain.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ConsumerRepository extends CrudRepository<Consumer, Long> {
}
